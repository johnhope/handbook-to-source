## GitLab Handbook to Source Extension

Jump to the source folder for any page in the GitLab handbook.


![How to use the extension once installed](./handbook-to-source.gif "Handbook to source")


### Installing

1. Clone this folder
1. In Chrome, go to chrome://extensions
1. Click 'Load Unpacked'
1. Select the folder you cloned the extension into