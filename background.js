// When the extension is installed or upgraded ...
chrome.runtime.onInstalled.addListener(function() {
  // Replace all rules ...
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    // With a new rule ...
    chrome.declarativeContent.onPageChanged.addRules([
      {
        // That fires when a page's URL contains handbook location
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { urlContains: 'about.gitlab.com/handbook' },
          })
        ],
        // And shows the extension's page action.
        actions: [ new chrome.declarativeContent.ShowPageAction() ]
      }
    ]);
  });

  actOnClick();
});

var actOnClick = function() {
  chrome.pageAction.onClicked.addListener(function(tab) {
    urlRoot = 'https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/source';
    currentUrl = new URL(tab.url);
    chrome.tabs.update({
      url: urlRoot + currentUrl.pathname
    });
  });
}

chrome.runtime.onStartup.addListener(actOnClick);
chrome.windows.onCreated.addListener(actOnClick);
chrome.windows.onFocusChanged.addListener(actOnClick);
